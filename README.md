# ACeBS Dashboard

ACeBS Dashboard menggunakan reactjs

## Struktur

```
	.
	├── bangunan-tahan-gempa #static info bangunan tahan gempa
	│	├── page
	│	│	├── 1
	│	│	│	├── 0.html
	│	│	│	└── ...
	│	│	├── 2
	│	│	│	├── 0.html
	│	│	│	└── ...
	│	│	├── 3
	│	│	│	├── 0.html
	│	│	│	└── ...
	│	│	└── 4
	│	│		├── 0.html
	│	│		└── ...
	│	├── src
	│	│	├── css
	│	│	│	└── ...
	│	│	├── images
	│	│	│	└── ...
	│	│	└── sass
	│	│		└── ...
	│	└── index.html
	│
	├── be #back end
	│	├── controller
	│	│	├── main.js
	│	│	└── user.js
	│	├── server.js
	│	└── ...
	│
	└── fe # front end
		├── public
		│	├── index.html
		│	└── ...
		├── src
		│	├── Auth
		│	│	├── Login.js
		│	│	├── Register.js
		│	│	└── Navbar.js
		│	├── Dashboard
		│	│	├── Dashboard.js
		│	│	├── Data.js
		│	│	├── DataTables.js
		│	│	├── Grafik.js
		│	│	└── NavbarFull.js
		│	└── Daerah
		│	├── Desa.js
		│	├── Kabupaten.js
		│	├── Kecamatan.js
		│	└── Propinsi.js
		├── App.js
		├── index.css
		└── ...
```

## Database User

```
users
	├── name
	├── username
	├── password
	├── token
	└── created_at
```

## Catatan Database

```
1. table abs_data kolom kerentanan_string bernilai 'Rendah' dirubah menjadi 'Tidak Rentan'
2. table abs_data kolom kerentanan_string bernilai 'Sedang' dirubah menjadi 'Rentan'
3. table abs_data kolom kerentanan_string bernilai 'Tinggi' dirubah menjadi 'Sangat Rentan'
```

## Front end route

```
acebs-dashboard
	├── signin
	├── register
	├── dashboard
	├── data
	└── grafik
```