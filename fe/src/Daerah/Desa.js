import React, { Component } from 'react'

class Desa extends Component {

  render() {

    const kabupatens = this.props.items.map(desa => {
      return (
		      <option key={desa.id} value={desa.id_desa}>{desa.nama_desa}</option>
        )
      })

    return (
      <select name="desaKode" className="form-control" id="_desa" onChange={this.props.onChange}>
		    <option value="">Desa</option>
		    {kabupatens}
      </select>
    )
  }
}

export default Desa