import React, { Component } from 'react'

class Kecamatan extends Component {

  render() {

    const kabupatens = this.props.items.map(kecamatan => {
      return (
		<option key={kecamatan.id} value={kecamatan.id_kecamatan}>{kecamatan.nama_kecamatan}</option>
        )
      })

    return (
      <select name="kecamatanKode" className="form-control" id="_kecamatan" onChange={this.props.onChange}>
		<option value="">Kecamatan</option>
		{kabupatens}
      </select>
    )
  }
}

export default Kecamatan