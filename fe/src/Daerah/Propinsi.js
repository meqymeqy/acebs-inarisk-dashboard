import React, { Component } from 'react'

class Propinsi extends Component {

  render() {

    const propinsis = this.props.items.map(propinsi => {
      return (
		<option key={propinsi.id} value={propinsi.id_provinsi}>{propinsi.nama_provinsi}</option>
        )
      })

    return (
      <select name="propinsiKode" className="form-control" id="_propinsi" onChange={this.props.onChange}>
		      <option value="">Propinsi</option>
		      {propinsis}
      </select>
    )
  }
}

export default Propinsi