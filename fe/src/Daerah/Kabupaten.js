import React, { Component } from 'react'

class Kabupaten extends Component {

  render() {

    const kabupatens = this.props.items.map(kabupaten => {
      return (
		<option key={kabupaten.id} value={kabupaten.id_kabupaten}>{kabupaten.nama_kabupaten}</option>
        )
      })

    return (
      <select name="kabupatenKode" className="form-control" id="_kabupaten" onChange={this.props.onChange}>
		<option value="">Kabupaten</option>
		{kabupatens}
      </select>
    )
  }
}

export default Kabupaten