import React from 'react'
import './App.css'

import { BrowserRouter, Route, Switch  } from "react-router-dom"

import Login from './Auth/Login'
import Register from './Auth/Register'

import Dashboard from './Dashboard/Dashboard'
import Data from './Dashboard/Data'
import Grafik from './Dashboard/Grafik'

function App() {
  return (
    <div className="App">
      <BrowserRouter basename="/acebs-dashboard">
        <Switch>
          <Route exact path="/" component={Dashboard}/>
          <Route path="/signin" component={Login}/>
          <Route path="/register" component={Register}/>
          <Route path="/dashboard" component={Dashboard}/>
          <Route path="/data" component={Data}/>
          <Route path="/grafik" component={Grafik}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
