import React, { Component } from "react"

import NavbarFull from './NavbarFull'
import DataTable from './DataTable'

class Data extends Component {

  state = {
    items: []
  }
  
	getItems(user_id){
		fetch('http://localhost:3005/dashboard/get-list',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				user_id
			})
		})
		.then(response => response.json())
		.then(items => {
			if (items.length) {
				this.setState({items})
			}else{
				this.setState({items : []})
			}
		})
		.catch(err => console.log(err))
	}

	componentDidMount(){
		this.getItems()
	}

  render() {
    return (
      <>
        <NavbarFull />
      	<div className="main-page">
					<div className="container">
						<DataTable items={this.state.items} updateState={this.updateState} deleteItemFromState={this.deleteItemFromState} />
					</div>
				</div>
      </>
    )
  }
}

export default Data