import React, { Component } from "react"
import { Card, Button, Row, Col } from 'react-bootstrap'

import NavbarFull from './NavbarFull'

import Propinsi from '../Daerah/Propinsi'
import Kabupaten from '../Daerah/Kabupaten'
import Kecamatan from '../Daerah/Kecamatan'
import Desa from '../Daerah/Desa'

import {Pie, HorizontalBar} from 'react-chartjs-2';

const alignCenter = {
	textAlign : 'left'
}

const buttonMargin = {
	marginRight : '10px'
}


class Grafik extends Component {

  constructor(props) {

    super(props);

    this.state = {
  		propinsiItem : [],
  		kabupatenItem : [],
  		kecamatanItem : [],
  		desaItem : [],
  		propinsiId : '',
  		kabupatenId : '',
  		kecamatanId : '',
  		desaId : '',
  		totalAssessment : 0,
  		totalTidakRentan : 0,
  		totalRentan : 0,
  		totalSangatRentan : 0,
  		chartData : {
				labels: [
					'Tidak Rentan',
					'Rentan',
					'Sangat Rentan'
				],
				datasets: [
					{
						data: [],
						backgroundColor: [
						'#00A65A',
						'#F39C12',
						'#DD4B39'
						],
						hoverBackgroundColor: [
						'#008548',
						'#C27D0E',
						'#B13C2E'
						]
					}
				]
			},
			barData : {
			  labels: [],
			  datasets: []
			}
		}

    this.onPropinsiChange = this.onPropinsiChange.bind(this)
    this.onKabupatenChange = this.onKabupatenChange.bind(this)
    this.onKecamatanChange = this.onKecamatanChange.bind(this)
    this.onDesaChange = this.onDesaChange.bind(this)

    this.resetGrafik = this.resetGrafik.bind(this)
    this.setGrafikBar = this.setGrafikBar.bind(this)
    this.getGrafikPropinsi = this.getGrafikPropinsi.bind(this)
  }
  
  onPropinsiChange = e =>{
	  this.setState({[e.target.name] : e.target.value, propinsiId : e.target.value})
	  this.getKabupaten(e.target.value)
  }
  
  onKabupatenChange = e =>{
	  this.setState({[e.target.name] : e.target.value, kabupatenId : e.target.value})
	  this.getKecamatan(e.target.value)
  }
  
  onKecamatanChange = e =>{
	  this.setState({[e.target.name] : e.target.value, kecamatanId : e.target.value})
	  this.getDesa(e.target.value)
  }
  
  onDesaChange = e =>{
	  this.setState({desaId : e.target.value})
  }

  getPropinsi(){
	  console.log('load propinsi');
	  fetch('http://localhost:3005/get-propinsi',{
      mode : 'cors',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
	  .then(items => { 
	  	this.setState({propinsiItem: items})
	  })
    .catch(err => console.log(err))
  }
  
  getKabupaten(id_provinsi){
	  if(id_provinsi!==""){
  		fetch('http://localhost:3005/get-kabupaten',{
  			method: 'post',
  			mode : 'cors',
  			headers: {
  				'Content-Type': 'application/json'
  			},
  			body: JSON.stringify({
  				id_provinsi
  			})
  		})
  		.then(response => response.json())
  		.then(items => {
  		  this.setState({kabupatenItem: items})})
  		.catch(err => console.log(err))
	  }else{
		  this.setState({kabupatenItem: []})
	  }

    this.setState({
      kecamatanItem: [],
      desaItem: [],
      kabupatenId : '',
  		kecamatanId : '',
  		desaId : ''
    })
  }
  
  getKecamatan(id_kabupaten){
	  console.log(id_kabupaten)
	  if(id_kabupaten!==""){
		fetch('http://localhost:3005/get-kecamatan',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				id_kabupaten
			})
		})
		.then(response => response.json())
		.then(items => {
			this.setState({kecamatanItem: items})})
		.catch(err => console.log(err))
	  }else{
		this.setState({kecamatanItem: []})
	  }
    
    this.setState({
      desaItem: [],
  		kecamatanId : '',
  		desaId : ''
    })
  }
  
  getDesa(id_kecamatan){
	  console.log(id_kecamatan)
	  if(id_kecamatan!==""){
			fetch('http://localhost:3005/get-desa',{
				method: 'post',
				mode : 'cors',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					id_kecamatan
				})
			})
			.then(response => response.json())
			.then(items => {
				this.setState({desaItem: items})})
			.catch(err => console.log(err))
		  }else{
			this.setState({desaItem: []})
	  }
    
    this.setState({
  		desaId : ''
    })
  }

  getGrafik= e =>{
  	if (this.state.desaId!=='') {
  		this.getGrafikDesa()
  	}else if (this.state.kecamatanId!=='') {
  		this.getGrafikKecamatan()
  	}else if (this.state.kabupatenId!=='') {
  		this.getGrafikKabupaten()
  	}else if (this.state.propinsiId!=='') {
  		this.getGrafikPropinsi()
  	}
  }

  resetGrafik(){
  	window.resetSelect()
  	fetch('http://localhost:3005/dashboard/get-grafik-all',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
		.then(items => {
			this.setState({
				kabupatenItem : [],
				kecamatanItem : [],
				desaItem : [],
				totalAssessment : items.total.assessment,
				totalTidakRentan : items.total.tidakrentan,
				totalRentan : items.total.rentan,
				totalSangatRentan : items.total.sangatrentan,
				chartData:{ 
					datasets: [
						{
							data: [items.total.tidakrentan, items.total.rentan, items.total.sangatrentan]
						}
					]
				}
			})

			var listPropinsi = []
			var tidakRentan = []
			var rentan = []
			var sangatRentan = []
			var a = 0
			items.propList.forEach(function (arrayItem) {
				var provinsi = arrayItem.nama_provinsi
				var tidakRentanString = arrayItem.tidakrentan
				var rentanString = arrayItem.rentan
				var sangatRentanString = arrayItem.sangatrentan

		    listPropinsi[a] = provinsi
		    tidakRentan[a] = tidakRentanString
		    rentan[a] = rentanString
		    sangatRentan[a] = sangatRentanString
		    a++
			})

			this.setGrafikBar(listPropinsi, tidakRentan, rentan, sangatRentan)
		})
		.catch(err => console.log(err))
  }

  getGrafikPropinsi(){
  	fetch('http://localhost:3005/dashboard/get-grafik-propinsi',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				prop : this.state.propinsiId
			})
		})
		.then(response => response.json())
		.then(items => {
			this.setState({
				totalAssessment : items.total.assessment,
				totalTidakRentan : items.total.tidakrentan,
				totalRentan : items.total.rentan,
				totalSangatRentan : items.total.sangatrentan,
				chartData:{ 
					datasets: [
							{
								data: [items.total.tidakrentan, items.total.rentan, items.total.sangatrentan]
							}
						]
					}
			})

			var listKabupaten = []
			var tidakRentan = []
			var rentan = []
			var sangatRentan = []
			var a = 0
			items.kabList.forEach(function (arrayItem) {
				var kabupaten = arrayItem.nama_kabupaten
				var tidakRentanString = arrayItem.tidakrentan
				var rentanString = arrayItem.rentan
				var sangatRentanString = arrayItem.sangatrentan

		    listKabupaten[a] = kabupaten
		    tidakRentan[a] = tidakRentanString
		    rentan[a] = rentanString
		    sangatRentan[a] = sangatRentanString
		    a++
			})

			this.setGrafikBar(listKabupaten, tidakRentan, rentan, sangatRentan)
		})
		.catch(err => console.log(err))
  }

  getGrafikKabupaten(){
  	fetch('http://localhost:3005/dashboard/get-grafik-kabupaten',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				kab : this.state.kabupatenId
			})
		})
		.then(response => response.json())
		.then(items => {
			this.setState({
				totalAssessment : items.total.assessment,
				totalTidakRentan : items.total.tidakrentan,
				totalRentan : items.total.rentan,
				totalSangatRentan : items.total.sangatrentan,
				chartData:{ 
					datasets: [
							{
								data: [items.total.tidakrentan, items.total.rentan, items.total.sangatrentan]
							}
						]
					}
			})

			var listKecamatan = []
			var tidakRentan = []
			var rentan = []
			var sangatRentan = []
			var a = 0
			items.kecList.forEach(function (arrayItem) {
				var kecamatan = arrayItem.nama_kecamatan
				var tidakRentanString = arrayItem.tidakrentan
				var rentanString = arrayItem.rentan
				var sangatRentanString = arrayItem.sangatrentan

		    listKecamatan[a] = kecamatan
		    tidakRentan[a] = tidakRentanString
		    rentan[a] = rentanString
		    sangatRentan[a] = sangatRentanString
		    a++
			})

			this.setGrafikBar(listKecamatan, tidakRentan, rentan, sangatRentan)
		})
		.catch(err => console.log(err))
  }

  getGrafikKecamatan(){
  	fetch('http://localhost:3005/dashboard/get-grafik-kecamatan',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				kec : this.state.kecamatanId
			})
		})
		.then(response => response.json())
		.then(items => {
			this.setState({
				totalAssessment : items.total.assessment,
				totalTidakRentan : items.total.tidakrentan,
				totalRentan : items.total.rentan,
				totalSangatRentan : items.total.sangatrentan,
				chartData:{ 
					datasets: [
							{
								data: [items.total.tidakrentan, items.total.rentan, items.total.sangatrentan]
							}
						]
					}
			})

			var listDesa = []
			var tidakRentan = []
			var rentan = []
			var sangatRentan = []
			var a = 0
			items.desList.forEach(function (arrayItem) {
				var desa = arrayItem.nama_desa
				var tidakRentanString = arrayItem.tidakrentan
				var rentanString = arrayItem.rentan
				var sangatRentanString = arrayItem.sangatrentan

		    listDesa[a] = desa
		    tidakRentan[a] = tidakRentanString
		    rentan[a] = rentanString
		    sangatRentan[a] = sangatRentanString
		    a++
			})

			this.setGrafikBar(listDesa, tidakRentan, rentan, sangatRentan)
		})
		.catch(err => console.log(err))
  }

  getGrafikDesa(){
  	fetch('http://localhost:3005/dashboard/get-grafik-desa',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				desa : this.state.desaId
			})
		})
		.then(response => response.json())
		.then(items => {
			this.setState({
				totalAssessment : items.total.assessment,
				totalTidakRentan : items.total.tidakrentan,
				totalRentan : items.total.rentan,
				totalSangatRentan : items.total.sangatrentan,
				chartData:{ 
					datasets: [
							{
								data: [items.total.tidakrentan, items.total.rentan, items.total.sangatrentan]
							}
						]
					}
			})

			this.setState({
				barData:{ 
					labels: [],
					datasets : []
				}
			})
		})
		.catch(err => console.log(err))
  }

  setGrafikBar(label, tidakRentan, rentan, sangatRentan){
  	this.setState({
				barData:{ 
					labels: label,
					datasets : [{
						label: 'Tidak Rentan',
			      backgroundColor: '#00A65A',
			      borderColor: '#008548',
			      borderWidth: 1,
			      hoverBackgroundColor: '#008548',
			      hoverBorderColor: '#008548',
						data : tidakRentan
					},{
			      label: 'Rentan',
			      backgroundColor: '#F39C12',
			      borderColor: '#C27D0E',
			      borderWidth: 1,
			      hoverBackgroundColor: '#C27D0E',
			      hoverBorderColor: '#C27D0E',
						data : rentan
					},{
			      label: 'Sangat Rentan',
			      backgroundColor: '#DD4B39',
			      borderColor: '#B13C2E',
			      borderWidth: 1,
			      hoverBackgroundColor: '#B13C2E',
			      hoverBorderColor: '#B13C2E',
						data : sangatRentan
					}]
				}
			})
  }

	componentDidMount(){
    this.getPropinsi()
    this.resetGrafik()
	}

	componentDidUpdate(){
    window.removePreloader()
  }

  render() {
    return (
      <>
        <NavbarFull />
      	<div className="main-page">
					<div className="container">
		        <Card className="card-default">
		          <Card.Header>Cari Berdasarkan Daerah</Card.Header>
		          <Card.Body>
		          	<Row>
		          		<Col lg={2}>
		          			<Propinsi items={this.state.propinsiItem} onChange={this.onPropinsiChange}/>
		          		</Col>
		          		<Col lg={2}>
		          			<Kabupaten items={this.state.kabupatenItem} onChange={this.onKabupatenChange}/>
		          		</Col>
		          		<Col lg={2}>
		          			<Kecamatan items={this.state.kecamatanItem} onChange={this.onKecamatanChange}/>
		          		</Col>
		          		<Col lg={2}>
		          			<Desa items={this.state.desaItem} onChange={this.onDesaChange}/>
		          		</Col>
		          		<Col lg={4} style={alignCenter}>
		          			<Button variant="success" onClick={this.getGrafik} style={buttonMargin}><i className="fa fa-search"></i> Search</Button>
		          			<Button variant="primary" onClick={this.resetGrafik}><i className="fa fa-times"></i> Reset</Button>
		          		</Col>
		          	</Row>
		          </Card.Body>
		        </Card>

		        <Row>
		        	<Col lg={8}>
		        		<Card className="card-grafik">
				          <Card.Header></Card.Header>
				          <Card.Body>
										<Pie data={this.state.chartData} />
				          </Card.Body>
				        </Card>
		        	</Col>
		        	<Col lg={4}>
			        	<Card className="card-default no-border">
			        		<Card.Body className="no-padding total-assessment-part">
			        			<Row>
			        				<Col lg={4} sm={4}>
			        					<div className="left-side">
				        					<i className="fa fa-send"></i>
				        				</div>
			        				</Col>
			        				<Col lg={8} sm={8}>
			        					<div className="right-side">
				        					<p>Total Assessment</p>
				        					<h5>{this.state.totalAssessment}</h5>
				        					</div>
			        				</Col>
			        			</Row>
				          </Card.Body>
				        </Card>

			        	<Card className="card-default no-border">
			        		<Card.Body className="no-padding total-tidak-rentan-part">
			        			<Row>
			        				<Col lg={4} sm={4}>
			        					<div className="left-side">
				        					<i className="fa fa-check"></i>
				        				</div>
			        				</Col>
			        				<Col lg={8} sm={8}>
			        					<div className="right-side">
				        					<p>Total Tidak Rentan</p>
				        					<h5>{this.state.totalTidakRentan}</h5>
				        					</div>
			        				</Col>
			        			</Row>
				          </Card.Body>
				        </Card>

			        	<Card className="card-default no-border">
			        		<Card.Body className="no-padding total-rentan-part">
			        			<Row>
			        				<Col lg={4} sm={4}>
			        					<div className="left-side">
				        					<i className="fa fa-warning"></i>
				        				</div>
			        				</Col>
			        				<Col lg={8} sm={8}>
			        					<div className="right-side">
				        					<p>Total Rentan</p>
				        					<h5>{this.state.totalRentan}</h5>
				        					</div>
			        				</Col>
			        			</Row>
				          </Card.Body>
				        </Card>

			        	<Card className="card-default no-border">
			        		<Card.Body className="no-padding total-sangat-rentan-part">
			        			<Row>
			        				<Col lg={4} sm={4}>
			        					<div className="left-side">
				        					<i className="fa fa-exclamation-circle"></i>
				        				</div>
			        				</Col>
			        				<Col lg={8} sm={8}>
			        					<div className="right-side">
				        					<p>Total Sangat Rentan</p>
				        					<h5>{this.state.totalSangatRentan}</h5>
				        					</div>
			        				</Col>
			        			</Row>
				          </Card.Body>
				        </Card>
		        	</Col>
		        </Row>
        		<Card className="card-grafik sub-card">
		          <Card.Header></Card.Header>
		          <Card.Body>
								<HorizontalBar data={this.state.barData} />
		          </Card.Body>
		        </Card>
					</div>
				</div>
      </>
    )
  }
}

export default Grafik