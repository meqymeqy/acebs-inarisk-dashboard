import React, { Component } from 'react'
import { Table } from 'reactstrap'
import { Card } from 'react-bootstrap'

class DataTable extends Component {

  convertDate(createdAt){
    var date = new Date(createdAt)
    var finalDate = new Intl.DateTimeFormat('id').format(date)
    return finalDate
  }

  cutString(str){
    var finalString
    if (str!=null) {
      finalString = str.substring(0, 20)
    }else{
      finalString = str
    }
    return finalString
  }

  componentDidUpdate(){
    window.sorting()
  }

  render() {

    var a = 0
    const items = this.props.items.map(item => {
      a++;
      return (
        <tr key={item.id}>
          <td className="fit">{a}</td>
          <td>{item.pemilik}</td>
          <td>{this.cutString(item.alamat)}...</td>
          <td className="fit">{item.nama_desa}</td>
          <td>{item.nama_kecamatan}</td>
          <td>{item.nama_kabupaten}</td>
          <td>{item.nama_provinsi}</td>
          <td className="fit">{item.kerentanan_string}</td>
          <td>{this.convertDate(item.created_at)}</td>
        </tr>
        )
      })

    return (
      <>
        <Card className="card-data">
          <Card.Header>List Data Assessment</Card.Header>
          <Card.Body>
            <Table responsive bordered className="dataTables">
              <thead>
                <tr>
                  <td>No</td>
                  <td>Nama Pemilik</td>
                  <td>Alamat</td>
                  <td>Desa</td>
                  <td>Kecamatan</td>
                  <td>Kab/Kota</td>
                  <td>Propinsi</td>
                  <td>Kerentanan</td>
                  <td className="fit">Tgl Assessment</td>
                </tr>
              </thead>
              <tbody>
                {items}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </>
    )
  }
}

export default DataTable