import React, { Component } from "react"
import { Redirect  } from "react-router-dom"
import { Container, Navbar, Nav } from 'react-bootstrap'
import {reactLocalStorage} from 'reactjs-localstorage'

class NavbarFull extends Component {

	constructor(props) {

    super(props);

    this.state = {
    	logout : false
    };

    this.onChange = this.onChange.bind(this)
  }

  onChange = e =>{
  	reactLocalStorage.clear()
  	this.setState({
  		logout : true
  	})
  }

  componentDidMount(){
		var user = reactLocalStorage.getObject('userData')
		if (!user.exist){
			this.setState({
	    	logout : true
			})
		}
	}
	
	componentDidUpdate(){
		window.removePreloader()
	}

  render() {
    return (
      <>
	      {this.state.logout ? <Redirect to='/signin' /> : null}
			  <Navbar className="acebs-navbar">
				  <Container>
				    <Navbar.Brand href="/acebs-dashboard/dashboard">Navbar</Navbar.Brand>
				    <Nav className="mr-auto">
				      <Nav.Link href="/acebs-dashboard/data">Data Assessment</Nav.Link>
				      <Nav.Link href="/acebs-dashboard/grafik">Grafik Assessment</Nav.Link>
				    </Nav>
				    <Nav>
				      <Nav.Link onClick={this.onChange}>Logout</Nav.Link>
				    </Nav>
			    </Container>
			  </Navbar>
			</>
    )
  }
}

export default NavbarFull