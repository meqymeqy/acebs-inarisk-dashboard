import React, { Component } from "react"
import { Redirect } from "react-router-dom"
import { Card, Row, Col } from 'react-bootstrap'
import { reactLocalStorage } from 'reactjs-localstorage'

import NavbarFull from './NavbarFull'

class Dashboard extends Component {

	constructor(props) {

    super(props);

    this.state = {
      name : '',
    };

    this.getData = this.getData.bind(this)
  }

  getData(){
  	var user = reactLocalStorage.getObject('userData');
  	if (user) {
  		this.setState({
  			name : user.name
  		})
  	}
  }

  componentDidMount(){
  	this.getData()
  }

  render() {
    return (
      <>
        <NavbarFull />
      	<div className="main-page">
					<div className="container">
						<Row className="justify-content-md-center">
					    <Col xs={8}>
					    	<Card className="vertical-center">
								  <Card.Header></Card.Header>
								  <Card.Body>
								  	<h5>Selamat Datang</h5>
								  </Card.Body>
								</Card>
					    </Col>
					  </Row>
					</div>
				</div>
      </>
    )
  }
}

export default Dashboard