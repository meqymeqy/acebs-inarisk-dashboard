import React, { Component } from "react"
import { Redirect  } from "react-router-dom"
import { Card, Button, Row, Col, Form } from 'react-bootstrap'
import {reactLocalStorage} from 'reactjs-localstorage'

import Navbar from './Navbar'

class Homepage extends Component {

	constructor(props) {

    super(props);

    this.state = {
    	login : false,
      username : '',
      password : '',
    };

    this.onChange = this.onChange.bind(this)
    this.submitLogin = this.submitLogin.bind(this)
  }

  onChange = e =>{
    this.setState({[e.target.name] : e.target.value})
    console.log([e.target.name] , e.target.value)
  }

  validate= e =>{
  	if (this.state.username!=='' && this.state.password!=='') {
  		this.submitLogin()
  	}else{
  		alert('harap lengkapi data anda')
  	}
  }

  submitLogin(){
  	fetch('http://localhost:3005/user/signin',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				username : this.state.username,
				password : this.state.password
			})
		})
		.then(response => response.json())
		.then(item => {
			if (item.exist) {
				reactLocalStorage.setObject('userData', {
					'exist' : true,
	        'name': item.user.name,
	        'username' : item.user.username,
	        'token' : item.user.token
	      })
				this.setState({
					login : true
				})
			}else{
				alert('user tidak ditemukan')
			}
		})
		.catch(err => console.log(err))
  }

	componentDidMount(){
		console.log('======================= homepage =====================')
		 window.removePreloader()
	}

	componentDidUpdate(){
    window.removePreloader()
  }

  render() {
    return (
      <>
	      {this.state.login ? <Redirect to='/dashboard' /> : null}
        <Navbar />
      	<div className="main-page">
					<div className="container">
						<Row className="justify-content-md-center">
					    <Col xs={8}>
					    	<Card>
								  <Card.Header>Login</Card.Header>
								  <Card.Body>
								  	<Form>
										  <Form.Group as={Row} controlId="name">
										    <Form.Label column sm="4" className="align-right">
										      Username
										    </Form.Label>
										    <Col sm="6">
										      <Form.Control name="username" type="text" placeholder="Username" onChange={this.onChange} value={this.state.username === null ? '' : this.state.username}/>
										    </Col>
										  </Form.Group>

										  <Form.Group as={Row} controlId="password">
										    <Form.Label column sm="4" className="align-right">
										      Password
										    </Form.Label>
										    <Col sm="6">
										      <Form.Control name="password" type="password" placeholder="Password" onChange={this.onChange} value={this.state.password === null ? '' : this.state.password} />
										    </Col>
										  </Form.Group>

										  <Form.Group as={Row} controlId="formPlaintextPassword">
										    <Form.Label column sm="4" className="align-right"></Form.Label>
										    <Col sm="6" className="align-left">
										      <Button variant="success" onClick={this.validate}>Login</Button>
										    </Col>
										  </Form.Group>
										</Form>
								  </Card.Body>
								</Card>
					    </Col>
					  </Row>
					</div>
				</div>
      </>
    )
  }
}

export default Homepage