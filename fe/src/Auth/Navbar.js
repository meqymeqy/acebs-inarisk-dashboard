import React, { Component } from "react"
import { Container, Navbar } from 'react-bootstrap'

class Homepage extends Component {

	componentDidMount(){
		console.log('======================= homepage =====================')
	}

  render() {
    return (
      <>
			  <Navbar className="acebs-navbar">
				  <Container className="navbar-center">
				    <Navbar.Brand href="/">ACeBS BNPB</Navbar.Brand>
			    </Container>
			  </Navbar>
			</>
    )
  }
}

export default Homepage