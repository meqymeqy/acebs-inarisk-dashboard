import React, { Component } from "react"
import { Redirect } from "react-router-dom"
import { Card, Button, Row, Col, Form } from 'react-bootstrap'

import Navbar from './Navbar'

class Homepage extends Component {

	constructor(props) {

    super(props);

    this.state = {
    	registerd : false,
      name : '',
      username : '',
      password : '',
    };

    this.onChange = this.onChange.bind(this)
    this.submitRegister = this.submitRegister.bind(this)
  }

  onChange = e =>{
    this.setState({[e.target.name] : e.target.value})
    console.log([e.target.name] , e.target.value)
  }

	componentDidMount(){
		console.log('======================= homepage =====================')
		 window.removePreloader()
	}

	componentDidUpdate(){
    window.removePreloader()
  }

  validate= e =>{
  	if (this.state.name!=='' && this.state.username!=='' && this.state.password!=='') {
  		this.submitRegister()
  	}else{
  		alert('harap lengkapi data anda')
  	}
  }

  submitRegister(){
  	fetch('http://localhost:3005/user/signup',{
			method: 'post',
			mode : 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name : this.state.name,
				username : this.state.username,
				password : this.state.password
			})
		})
		.then(response => response.json())
		.then(item => {
			if (item.exist) {
				alert('username telah tersedia, silahkan login')
			}else{
				if (window.confirm('Registrasi Berhasil, Silahkan Login')){
				  this.setState({
							registerd : true
					})
				}
			}
		})
		.catch(err => console.log(err))
  }

  render() {
    return (
      <>
        <Navbar />
        {this.state.registerd ? <Redirect to='/' /> : null}
      	<div className="main-page">
					<div className="container">
						<Row className="justify-content-md-center">
					    <Col xs={8}>
					    	<Card>
								  <Card.Header>Register</Card.Header>
								  <Card.Body>
								  	<Form>
										  <Form.Group as={Row} controlId="name">
										    <Form.Label column sm="4" className="align-right">
										      Name
										    </Form.Label>
										    <Col sm="6">
										      <Form.Control name="name" type="text" placeholder="Name" onChange={this.onChange} value={this.state.name === null ? '' : this.state.name}/>
										    </Col>
										  </Form.Group>

										  <Form.Group as={Row} controlId="username">
										    <Form.Label column sm="4" className="align-right">
										      Username
										    </Form.Label>
										    <Col sm="6">
										      <Form.Control name="username" type="text" placeholder="Username" onChange={this.onChange} value={this.state.username === null ? '' : this.state.username}/>
										    </Col>
										  </Form.Group>

										  <Form.Group as={Row} controlId="password">
										    <Form.Label column sm="4" className="align-right">
										      Password
										    </Form.Label>
										    <Col sm="6">
										      <Form.Control name="password" type="password" placeholder="Password" onChange={this.onChange} value={this.state.password === null ? '' : this.state.password} />
										    </Col>
										  </Form.Group>

										  <Form.Group as={Row}>
										    <Form.Label column sm="4" className="align-right"></Form.Label>
										    <Col sm="6" className="align-left">
										      <Button variant="success" type="button" onClick={this.validate}>Register</Button>
										    </Col>
										  </Form.Group>
										</Form>
								  </Card.Body>
								</Card>
					    </Col>
					  </Row>
					</div>
				</div>
      </>
    )
  }
}

export default Homepage