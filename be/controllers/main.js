const crypto = require('crypto');


const getPropinsi = (req, res, db) => {
  db.select('*').from('propinsis')
    .then(propinsis => {
      if(propinsis.length){
        res.json(propinsis)
      } else {
        res.json({dataExists: 'false'})
      }
    })
    .catch(err => res.status(400).json({dbError: 'db error'}))
}

const getKabupaten = (req, res, db) => {
  const { id_provinsi} = req.body
  db.select('*').from('kabupatens').where({id_provinsi})
    .then(propinsis => {
      if(propinsis.length){
        res.json(propinsis)
      } else {
        res.json({dataExists: 'false'})
      }
    })
    .catch(err => res.status(400).json({dbError: 'db error'}))
}

const getKecamatan = (req, res, db) => {
  const { id_kabupaten} = req.body
  db.select('*').from('kecamatans').where({id_kabupaten})
    .then(kecamatans => {
      if(kecamatans.length){
        res.json(kecamatans)
      } else {
        res.json({dataExists: 'false'})
      }
    })
    .catch(err => res.status(400).json({dbError: 'db error'}))
}

const getDesa = (req, res, db) => {
  const { id_kecamatan } = req.body
  db.select('*').from('desa').where({id_kecamatan })
    .then(kecamatans => {
      if(kecamatans.length){
        res.json(kecamatans)
      } else {
        res.json({dataExists: 'false'})
      }
    })
    .catch(err => res.status(400).json({dbError: 'db error'}))
}

const getListDataDashboard = (req, res, db) => {
  // const { user_id, building_type_id } = req.body
  db.select(
      'abs_data.id',
      'abs_data.pemilik',
      'abs_data.kerentanan_string',
      'abs_data.alamat',
      'abs_data.created_at',
      'desa.nama_desa',
      'kecamatans.nama_kecamatan',
      'kabupatens.nama_kabupaten',
      'propinsis.nama_provinsi'
      )
    .from('abs_data')
    .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
    .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
    .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
    .innerJoin('propinsis', 'kabupatens.id_provinsi', '=', 'propinsis.id_provinsi')
    // .where({ 'user_id': user_id, 'abs_data.active': true})
    .then(items => {
      if(items.length){
        res.json(items)
      } else {
        res.json({dataExists: 'false'})
      }
    })
    .catch(err => res.status(400).json({dbError: err}))
}

const getAllGrafik = (req, res, db) => {
  db.select(
      db.raw("SUM(case when abs_data.kerentanan_string!='' or abs_data.kerentanan_string!=null then 1 else 0 end) as assessment"),
      db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan")
      )
    .from('abs_data')
    .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
    .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
    .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
    .innerJoin('propinsis', 'kabupatens.id_provinsi', '=', 'propinsis.id_provinsi')
    .then(totals => {

      db.select(
          db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan"),
          'propinsis.nama_provinsi',
          'propinsis.id'
        )
        .from('abs_data')
        .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
        .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
        .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
        .innerJoin('propinsis', 'kabupatens.id_provinsi', '=', 'propinsis.id_provinsi')
        .groupBy('propinsis.nama_provinsi', 'propinsis.id')
        .orderBy('propinsis.id', 'asc')
        .then(propList => {

          var total = totals[0]
          var data = { total, propList }
          res.json(data)
      })
    })
    .catch(err => res.status(400).json({dbError: err}))
}

const getPropGrafik = (req, res, db) => {
  const { prop } = req.body
  db.select(
      db.raw("SUM(case when abs_data.kerentanan_string!='' or abs_data.kerentanan_string!=null then 1 else 0 end) as assessment"),
      db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan")
    )
    .from('abs_data')
    .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
    .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
    .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
    .innerJoin('propinsis', 'kabupatens.id_provinsi', '=', 'propinsis.id_provinsi')
    .where({'propinsis.id_provinsi' : prop})
    .then(totals => {

      db.select(
          db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan"),
        'kabupatens.nama_kabupaten',
        'kabupatens.id'
        )
        .from('abs_data')
        .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
        .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
        .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
        .innerJoin('propinsis', 'kabupatens.id_provinsi', '=', 'propinsis.id_provinsi')
        .groupBy('kabupatens.nama_kabupaten', 'kabupatens.id')
        .orderBy('kabupatens.id', 'asc')
        .where({'propinsis.id_provinsi' : prop})
        .then(kabList => {

          var total = totals[0]
          var data = { total, kabList }
          res.json(data)
      })
    })
    .catch(err => res.status(400).json({dbError: err}))
}

const getKabGrafik = (req, res, db) => {
  const { kab } = req.body
  db.select(
      db.raw("SUM(case when abs_data.kerentanan_string!='' or abs_data.kerentanan_string!=null then 1 else 0 end) as assessment"),
      db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan")
    )
    .from('abs_data')
    .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
    .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
    .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
    .where({'kabupatens.id_kabupaten' : kab})
    .then(totals => {

      db.select(
          db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan"),
        'kecamatans.nama_kecamatan',
        'kecamatans.id'
        )
        .from('abs_data')
        .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
        .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
        .innerJoin('kabupatens', 'kecamatans.id_kabupaten', '=', 'kabupatens.id_kabupaten')
        .groupBy('kecamatans.nama_kecamatan', 'kecamatans.id')
        .orderBy('kecamatans.id', 'asc')
        .where({'kabupatens.id_kabupaten' : kab})
        .then(kecList => {

          var total = totals[0]
          var data = { total, kecList }
          res.json(data)
      })
    })
    .catch(err => res.status(400).json({dbError: err}))
}

const getKecGrafik = (req, res, db) => {
  const { kec } = req.body
  db.select(
      db.raw("SUM(case when abs_data.kerentanan_string!='' or abs_data.kerentanan_string!=null then 1 else 0 end) as assessment"),
      db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan")
    )
    .from('abs_data')
    .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
    .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
    .where({'kecamatans.id_kecamatan' : kec})
    .then(totals => {

      db.select(
          db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
          db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan"),
        'desa.nama_desa',
        'desa.id'
        )
        .from('abs_data')
        .innerJoin('desa', 'abs_data.id_desa', '=', 'desa.id_desa')
        .innerJoin('kecamatans', 'desa.id_kecamatan', '=', 'kecamatans.id_kecamatan')
        .groupBy('desa.nama_desa', 'desa.id')
        .orderBy('desa.id', 'asc')
        .where({'desa.id_kecamatan' : kec})
        .then(desList => {

          var total = totals[0]
          var data = { total, desList }
          res.json(data)
      })
    })
    .catch(err => res.status(400).json({dbError: err}))
}

const getDesaGrafik = (req, res, db) => {
  const { desa } = req.body
  db.select(
      db.raw("SUM(case when abs_data.kerentanan_string!='' or abs_data.kerentanan_string!=null then 1 else 0 end) as assessment"),
      db.raw("SUM(case when abs_data.kerentanan_string='Tidak Rentan' then 1 else 0 end) as tidakrentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Rentan' then 1 else 0 end) as rentan"),
      db.raw("SUM(case when abs_data.kerentanan_string='Sangat Rentan' then 1 else 0 end) as sangatrentan")
    )
    .from('abs_data')
    .where({'id_desa' : desa})
    .then(totals => {
          var total = totals[0]
          var data = { total }
          res.json(data)
    })
    .catch(err => res.status(400).json({dbError: err}))
}

module.exports = {
  getPropinsi,
  getKabupaten,
  getKecamatan,
  getDesa,

  getListDataDashboard,

  getAllGrafik,
  getPropGrafik,
  getKabGrafik,
  getKecGrafik,
  getDesaGrafik
}