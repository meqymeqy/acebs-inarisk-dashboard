const bcrypt          = require('bcrypt')
const crypto          = require('crypto')


const signup = (req, res, db) => {
  const user = req.body
		checkingUser(user, db)
	  .then((check) =>{
	  	// res.json(check)
	  	if (check) {
	  		check.exist = true
	  		res.json(check)
	  	}else{
	  		hashPassword(user.password)
				 .then((hashedPassword) => {
				   delete password
				   user.password = hashedPassword
				 })
				 .then(() => createToken())
				 .then(token => user.token = token)
				 .then(() => createUser(user, db))
				 .then(user => {
				   delete user.password
				   user.exist = false
				   res.json({ user })
				 })
				 .catch((err) => console.error(err))
	  	}
	  })
    .catch((err) => console.error(err))
}

const checkingUser =(user, db)=>{
	return db.select('*')
    .from('users')
    .where({'username' : user.username})
    .then((data) => data[0])
}

const hashPassword = (password) => {
  return new Promise((resolve, reject) =>
    bcrypt.hash(password, 10, (err, hash) => {
      err ? reject(err) : resolve(hash)
    })
  )
}

const createUser = (user, db) => {
	return db('users').insert({
		name : user.name,
		username : user.username,
		password : user.password,
		token : user.token,
		created_at : new Date()
	})
	.returning('*')
	.then((data) => data[0])
}

const createToken = () => {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(16, (err, data) => {
      err ? reject(err) : resolve(data.toString('base64'))
    })
  })
}

const signin = (req, res, db) => {
  const userReq = req.body
  console.log('user', userReq)
  let user

  findUser(userReq, db)
    .then(foundUser => {
    	if (foundUser) {
    		user = foundUser
	      return checkPassword(userReq.password, foundUser)
	      .then((res) => createToken())
		    .then(token => updateUserToken(token, user, db))
		    .then(() => {
		      delete user.password
		     	var exist = true
		      var data = {user, exist}
		      res.json(data)
		    })
		    .catch((err) => console.error(err))
    	}else{
    		var exist = false
    		var data = {exist}
    		res.json(data)
    	}
    })
    .catch((err) => console.error(err))
    
}

const findUser = (userReq, db) => {
  return db.select('*')
    .from('users')
    .where({'username' : userReq.username})
    .then((data) => data[0])

}

const checkPassword = (reqPassword, foundUser) => {
  return new Promise((resolve, reject) =>
    bcrypt.compare(reqPassword, foundUser.password, (err, response) => {
        if (err) {
          reject(err)
        }
        else if (response) {
          resolve(response)
        } else {
          reject(new Error('Passwords do not match.'))
        }
    })
  )
}

const updateUserToken = (token, user, db) => {
  return db.raw("UPDATE users SET token = ? WHERE id = ? RETURNING id, username, token", [token, user.id])
    .then((data) => data.rows[0])
}

const authenticate = (userReq) => {
  findByToken(userReq.token)
    .then((user) => {
      if (user.username == userReq.username) {
        return true
      } else {
        return false
      }
    })
}

const findByToken = (token) => {
  return database.raw("SELECT * FROM users WHERE token = ?", [token])
    .then((data) => data.rows[0])
}

module.exports = {
	signup,
	signin
}