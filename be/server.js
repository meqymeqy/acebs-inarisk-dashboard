const express = require('express')

// use process.env variables to keep private variables,
require('dotenv').config()

// Express Middleware
const helmet = require('helmet') // creates headers that protect from attacks (security)
const bodyParser = require('body-parser') // turns response into usable format
const cors = require('cors')  // allows/disallows cross-site communication
const morgan = require('morgan') // logs requests

// db Connection w/ localhost
var db = require('knex', 'Promise')({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'root',
    password : 'supersekali',
    database : 'acebs_inarisk'
  }
});

// Controllers - aka, the db queries
const main = require('./controllers/main')
const user = require('./controllers/user')

// App
const app = express()

// App Middleware
const whitelist = ['http://localhost:3007']
const corsOptions = {
  origin: function (origin, callback) {
    // callback(null, true)
    if (whitelist.indexOf(origin) !== -1 || whitelist2.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
      // callback(null, true)
    }
  }
}
app.use(helmet())
app.use(cors(corsOptions))
app.use(bodyParser.json())
app.use(morgan('combined')) // use 'tiny' or 'combined'

// App Routes - Auth
app.get('/', (req, res) => res.send('hello world'))

app.get('/get-propinsi', (req, res) => main.getPropinsi(req, res, db)) //
app.post('/get-kabupaten', (req, res) => main.getKabupaten(req, res, db)) //
app.post('/get-kecamatan', (req, res) => main.getKecamatan(req, res, db)) //
app.post('/get-desa', (req, res) => main.getDesa(req, res, db)) //

app.post('/user/signup', (req, res) => user.signup(req, res, db)) //
app.post('/user/signin', (req, res) => user.signin(req, res, db)) //

app.post('/dashboard/get-list', (req, res) => main.getListDataDashboard(req, res, db))
app.post('/dashboard/get-grafik-all', (req, res) => main.getAllGrafik(req, res, db))
app.post('/dashboard/get-grafik-propinsi', (req, res) => main.getPropGrafik(req, res, db))
app.post('/dashboard/get-grafik-kabupaten', (req, res) => main.getKabGrafik(req, res, db))
app.post('/dashboard/get-grafik-kecamatan', (req, res) => main.getKecGrafik(req, res, db))
app.post('/dashboard/get-grafik-desa', (req, res) => main.getDesaGrafik(req, res, db))

// App Server Connection
app.listen(process.env.PORT || 3005, () => {
  console.log(`app is running on port ${process.env.PORT || 3005}`)
})